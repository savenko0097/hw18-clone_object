"use strict";

function Clone(obj) {
  if (obj === null || typeof obj !== 'object') {
    return obj;
  }

  let clonedObj = Array.isArray(obj) ? [] : {};

  for (let key in obj) {
    clonedObj[key] = Clone(obj[key]);
  }

  return console.log(clonedObj);
}


let obj = {
  name: "Anastasiia",
  lastname: "Savenko",
  adress: {
    country: "Ukraine", 
    city: "Kyiv",
  }, 
  grade: [100, 80, 90, 100]

}

Clone(obj);